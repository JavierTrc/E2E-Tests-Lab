module.exports = { // adapted from: https://git.io/vodU0
    'Los estudiantes login falied': function(browser) {
      browser
        .url('https://losestudiantes.co/')
        .click('.botonCerrar')
        .waitForElementVisible('.botonIngresar', 4000)
        .click('.botonIngresar')
        .setValue('.cajaLogIn input[name="correo"]', 'wrongemail@example.com')
        .setValue('.cajaLogIn input[name="password"]', '1234')
        .click('.cajaLogIn .logInButton')
        .waitForElementVisible('.aviso.alert.alert-danger', 4000)
        .assert.containsText('.aviso.alert.alert-danger', 'El correo y la contraseña que ingresaste no figuran')
        .end();
    },
    'Los estudiantes search bar finds the teachers': function(browser) {
        browser
            .url("https://losestudiantes.co")
            .click('.botonCerrar')
            .pause(500)
            .setValue('.Select-input input', 'Mario Linares')
            .waitForElementVisible('.Select-option', 2000)
            .assert.containsText('.Select-option', 'Mario Linares Vasquez - Ingeniería De Sistemas')
            .end()
    },
    'Los estudiantes seacrh bar doesn\' find the teacher': function(browser) {
        browser
            .url("https://losestudiantes.co")
            .click('.botonCerrar')
            .pause(500)
            .setValue('.Select-input input', 'Non Existent Proffesor')
            .waitForElementVisible('.Select-noresults', 2000)
            .pause(1000)
            .assert.containsText('.Select-noresults', 'No se encontraron profesores ni materias')
            .end()
    },
    'Los estudiantes teacher page subject filter works correctly': function(browser) {
        var total = '17'
        var movilesId = 'id:ISIS3510'
        var target = { width: 1400, height: 9000 };

        browser
            .resizeWindow(target.width, target.height)
            .url('https://losestudiantes.co/universidad-de-los-andes/ingenieria-de-sistemas/profesores/mario-linares-vasquez')
            .assert.containsText('#profesor_cantidad', total)
            .click(`.materias input[name="${movilesId}"]`)
            .pause(1000)
            .assert.containsText('#profesor_cantidad', '6')
            .click('.materias input[type="checkbox"]')
            .assert.containsText('#profesor_cantidad', total)
            .end()
    }
  };