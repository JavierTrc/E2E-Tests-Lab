describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()

        cy.contains('Ingresar').click()
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("wrongemail@example.com")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("1234")
        cy.get('.cajaLogIn').contains('Ingresar').click()
        cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.')
    })

    it('Visits los estudiantes and logins correctly', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        
        cy.contains('Ingresar').click()
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("javotrc@gmail.com")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("Contraseña")
        cy.get('.cajaLogIn').contains('Ingresar').click()

        cy.get('#cuenta')
    })
})

describe('Los estudiantes create account', function() {
    it('Fails with a mail that already exists', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        
        cy.contains('Ingresar').click()
        
        cy.get('.cajaSignUp').find('input[name="nombre"]').click().type('Javier')
        cy.get('.cajaSignUp').find('input[name="apellido"]').click().type('Troconis')
        cy.get('.cajaSignUp').find('input[name="correo"]').click().type('javotrc@gmail.com')
        cy.get('.cajaSignUp').find('select[name="idPrograma"]').select("Diseño")
        cy.get('.cajaSignUp').find('input[name="password"]').click().type('12345678')
        cy.get('.cajaSignUp').find('input[name="acepta"]').click()
        cy.get('.cajaSignUp').contains('Registrarse').click()

        var errorMessage = 'Error: Ya existe un usuario registrado con el correo \'javotrc@gmail.com\''
        cy.get('.sweet-alert').contains(errorMessage)
    })
})

describe('Los estudiantes search teacher bar', function() {

    beforeEach(function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
    })

    it('Search correctly for an existing proffesor', function() {
        cy.get(`div.Select-control input`).type('Mario Linares', {force:true})
        cy.contains('Mario Linares Vasquez')
    })

    it('Search correctly for non existing professor', function() {
        cy.get(`div.Select-control input`).type('Non-Existent', {force:true})
        cy.contains('No se encontraron profesores ni materias')
    })
})

describe('Los estudiantes go to teacher page', function() {
    beforeEach(function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
    })

    it('Navigates from a click to a teacher', function() {
        cy.get('.profesor a').first().click()
        cy.get('.infoProfesor')
    })

    it('Navigates from a click from a teacher in the search bar', function(){
        cy.get(`div.Select-control input`).type('Mario Linares', {force:true})
        cy.contains('Mario Linares Vasquez').click()
        cy.get('.infoProfesor')
    })
})

describe('Los estudiantes teacher page subject filter', function(){
    beforeEach(function() {
        cy.visit('https://losestudiantes.co/universidad-de-los-andes/ingenieria-de-sistemas/profesores/mario-linares-vasquez')
    })

    it('Filters correctly the number of reviews', function(){

        let total = '17'

        let movilesId = 'id:ISIS3510'

        cy.get('#profesor_cantidad').contains(total)

        cy.get('.materias').find(`input[name="${movilesId}"]`).click()
        cy.get('#profesor_cantidad').contains('6')

        cy.get('.materias').find('input[type="checkbox"]').check()
        cy.get('#profesor_cantidad').contains(total)
    })
})