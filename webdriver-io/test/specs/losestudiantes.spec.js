var assert = require('assert');
describe('los estudiantes login', function() {
    it('should visit los estudiantes and failed at log in', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');

        var cajaLogIn = browser.element('.cajaLogIn');
        var mailInput = cajaLogIn.element('input[name="correo"]');

        mailInput.click();
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');

        passwordInput.click();
        passwordInput.keys('1234');

        cajaLogIn.element('button=Ingresar').click()
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);

        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).toBe('Upss! El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.');
    });

    it('Visits los estudiantes and logins correctly', function() {
        browser.url('https://losestudiantes.co')
        browser.click('button=Ingresar');
        
        var cajaLogIn = browser.element('.cajaLogIn');

        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('javotrc@gmail.com');

        var passwordInput = cajaLogIn.element('input[name="password"]')
        passwordInput.click();
        passwordInput.keys('Contraseña');

        cajaLogIn.element('button=Ingresar').click();

        expect(browser.element('#cuenta')).toBeDefined();
    })
});

describe('Los estudiantes create account', function() {
    it('Fails with a mail that already exists', function() {
        browser.url('https://losestudiantes.co')        
        browser.click('button=Ingresar');
        
        var cajaSignUp = browser.element('.cajaSignUp');

        cajaSignUp.element('input[name="nombre"]').keys('Javier')
        cajaSignUp.element('input[name="apellido"]').keys('Troconis')
        cajaSignUp.element('input[name="correo"]').keys('javotrc@gmail.com')
        cajaSignUp.element('select[name="idPrograma"]').selectByVisibleText("Diseño")
        cajaSignUp.element('input[name="password"]').keys('12345678')
        cajaSignUp.click('input[name="acepta"]')
        cajaSignUp.click('button=Registrarse')

        var errorMessage = 'Error: Ya existe un usuario registrado con el correo \'javotrc@gmail.com\''
        expect(browser.element(`=${errorMessage}`)).toBeDefined();
    })
});

describe('Los estudiantes search teacher bar', function() {

    beforeEach(function() {
        browser.url('https://losestudiantes.co')
    })

    it('Search correctly for an existing proffesor', function() {
        browser.element(`div.Select-control input`).keys('Mario Linares')
        expect(browser.element('*=Mario Linares Vasquez'))
    })

    it('Search correctly for non existing professor', function() {
        browser.element(`div.Select-control input`).keys('Non-Existent')
        expect(browser.element('*=No se encontraron profesores ni materias')).toBeDefined();
    })
})